package urlshortener

import (
	"crypto/sha1"
	"encoding/hex"
	"testing"
)

func TestUrlShortener(t *testing.T) {
	url := "otus.ru/some-long-link"
	us := NewUrlShortener(hash)
	if shorten := us.Shorten(url); shorten == url {
		t.Errorf("shorten url equals base url")
	}
	shorten := us.Shorten(url)
	if resolved, _ := us.Resolve(shorten); resolved != url {
		t.Errorf("resolved url not equals base url")
	}
}

func hash(string string) string {
	hasher := sha1.New()
	hasher.Write([]byte(string))

	return hex.EncodeToString(hasher.Sum(nil))[0:5]
}
