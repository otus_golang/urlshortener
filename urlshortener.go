package urlshortener

import "sync"

type Shortener interface {
	Shorten(string) string
	Resolve(string) string
}

type UrlShortener struct {
	mx               sync.RWMutex
	data             map[string]string
	shortenAlgorithm func(path string) string
}

func NewUrlShortener(shortenAlgorithm func(path string) string) *UrlShortener {
	return &UrlShortener{
		data:             make(map[string]string),
		shortenAlgorithm: shortenAlgorithm,
	}
}

func (s *UrlShortener) Shorten(url string) string {
	s.mx.Lock()
	defer s.mx.Unlock()
	shortUrl := s.shortenAlgorithm(url)
	if val, isSet := s.data[shortUrl]; isSet {
		if val == url {
			return shortUrl
		}

		for isSet {
			shortUrl = s.shortenAlgorithm(shortUrl)
			_, isSet = s.data[shortUrl]
		}

		s.data[shortUrl] = url
		return shortUrl

	}
	s.data[shortUrl] = url

	return shortUrl
}

func (s *UrlShortener) Resolve(url string) (string, bool) {
	s.mx.RLock()
	defer s.mx.RUnlock()
	val, ok := s.data[url]
	return val, ok
}
